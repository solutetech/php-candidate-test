<?php
/**
 * Convert numbers to Roman Numerals
 * Allow the ability to set custom letters for 1 and 5
 * 
 */
class Roman
{
    protected $romans;
    
    public function __construct()
    {
        $this->romans = [1000 => 'M', 900 => 'CM', 500 => 'D', 400 => 'CD', 100 => 'C', 90 => 'XC', 50 => 'L', 40 => 'XL', 10 => 'X', 9 => 'IX', 5 => 'V', 4 => 'IV', 1 => 'I'];
    }
    
    public function setter($num, $letter) {
        if($num == 1 || $num == 5) {
            $this->romans[$num] = $letter;
        }
    }
    
    public function output ($number)
    {
        $numeral = '';

        foreach($this->romans as $num => $letter){
            while ($number >= $num) {
                $numeral .= $letter;
                $number -= $num;
            }
        }

        return $numeral ."\n";
    }
}