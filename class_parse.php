<?php
/**
 * Parse a CSV, JSON, or XML
 */
class Parser
{
    private $data;
 
    public function __construct($file)
    {
        if(!file_exists($file)) die('File does not exist.');
        
        $ext = new SplFileInfo($file);
        
        switch($ext->getExtension()) {
            case 'csv':
                $data = $this->csv($file);
                break;
            case 'json':
                $data = $this->json($file);
                break;
            case 'xml':
                $data = $this->xml($file);
                break;
            default:
                die('Invalid File Type');
        }
        
        $this->data = $data;
    }
    
    // Parse CSV file
    public function csv($file) {
        $csv = array();
        $lines = file($file, FILE_IGNORE_NEW_LINES);
        foreach ($lines as $key => $value)
        {
            $csv[] = str_getcsv($value);
        }
        return $csv;
    }
    
    // Parse JSON data
    public function json($file) {
        $values = file_get_contents($file);
        return json_decode($values, true);
    }
    
    // Parse XML file
    public function xml($file) {
        $arr = array();
        $values = file_get_contents($file);
        $xml = simplexml_load_string($values, "SimpleXMLElement", LIBXML_NOCDATA);
        $json = json_decode(json_encode($xml), true);
        for($i=0; $i<count($json['row']); $i++) {
            foreach($json['row'][$i] as $key => $value) {
                $arr[$i][] = $value;
            }
        };
        return $arr;
    }
    
    // Parse the data
    public function parser() {
        if(isset($this->data) || !empty($this->data) ) {
            foreach($this->data as $v) {
                echo $v[0] . ' ' . $v[1] . ' ('. $v[2] .')' . "\n";
                $cat = explode(";", $v[3]);
                foreach($cat as $c) {
                    echo '- '. $c . "\n";
                }
                echo "\n";
            }
        } else {
            die('No data loaded.');
        }
    }
}