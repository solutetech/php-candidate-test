<?php
/*
 *  1. Refactor the following pseudo code, correct if necessary (a little confusing as this is not pseudo code)
 *
 *  My thoughts of Pseudo Code
 *  Set id equal to the global variable id.
 *  Using the conn database connection, select everything from the testdb table where the id field equals the value of the id variable and assign the results to the result variable 
 *
 *  I would use PDO prepared statements
 *  $id = $request['id']; // This line is suspect. I'm thinking this should be coming from either $_GET['id'], $_POST['id'], or $_REQUEST['id'].
 *  $result = $pdo->prepare('SELECT * FROM testdb WHERE id = :id');
 *  $result->execute( array('id'=>$id) );
 *
 */
 
 
 
/* 2. Given the following, What is the value of $b, what is the value of $a, why is that the value?
 *
 *  function doSomething ( &$arg )
 *  {
 *      $return = $arg;
 *      $arg += 1;
 *      return $return;
 *  }
 *  
 *  $a = 3;
 *  $b = doSomething( $a );
 *
 *  a)  $b will equal 3.
 *  b)  $a will equal 4.
 *  
 *  c)  $b is equal to 3 because $a, which initially equals 3, is passed to the function and is assigned to $return and $return is "returned".
 *  $a will now equal 4 because $a was passed by reference (meaning the actual variable $a is manipulated instead of a copy of it) and is incremented by 1. 
 *
 */
    
    
    
/*
 *  3. Parse the included file and output in the format below.  This should be a command line tool.   
 *  Create the parsing code as a library so that it may be used by other applications (Hint: OOP).
 *  Bonus (optional): Design so that other file formats (xml, json) can be used in the future with ease.
 *
 *  Run "php index.php items.fileExtension" either csv, json, or xml 
 */
 
 if( isset($argv[1]) ) {
    require 'class_parse.php';
    $parse = new Parser($argv[1]);
    $parse->parser();
 }
 
 
 
 /*
  *  4. Implement a function to convert an integer to roman numeral function.  
  *  It should allow a custom format for 1 and 5 (e.g. 1=Z, 5=P). 
  *  Bonus (optional) Use an object oriented approach.
  *
  */
  require 'class_roman.php';
  $roman = new Roman();
  print_r($roman->output(2395));
  
  $roman->setter(100, 'P');
  print_r($roman->output(2395));
  
  print_r($roman->output(1201));
  
  $roman->setter(1, 'Z');
  print_r($roman->output(1201));
  
 ?>
